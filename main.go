package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/samandarobidovfrd/cdci-with-go/api"
)

func main() {
	server := gin.Default()

	server.GET("/", api.HomeController)
	server.GET("/ping", api.PongGetController)
	server.GET("/hello/:name", api.HelloGetController)

	server.Run(":8088")
}
