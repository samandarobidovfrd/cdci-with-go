package api

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func PongGetController(c *gin.Context) {
	c.JSON(200, gin.H{
		"error":   false,
		"message": "pong",
	})
}

func HelloGetController(c *gin.Context) {
	name := c.Param("name")
	if len(name) == 0 {
		c.JSON(400, gin.H{
			"error":   true,
			"message": "Please enter your name!",
		})
	}

	helloString := fmt.Sprintf("Hello %v", name)

	c.JSON(200, gin.H{
		"error":   false,
		"message": helloString,
	})
}

func HomeController(c *gin.Context) {

	c.JSON(200, gin.H{
		"error":  false,
		"status": "OK",
	})
}
