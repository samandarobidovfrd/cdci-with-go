CURRENT_DIR=$(shell pwd)

APP=$(shell basename ${CURRENT_DIR})

IMG_NAME=${APP}
REGISTRY=${REGISTRY}

TAG=latest
ENV_TAG=latest

include .build_info

build:
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o bin/cicd-with-go

build-image:
	docker build --rm -t ${REGISTRY}/${PROJECT_NAME}/cdci-with-go:latest .
	docker tag ${REGISTRY}/${PROJECT_NAME}/cdci-with-go:latest ${REGISTRY}/${PROJECT_NAME}:latest

push-image:
	docker push ${REGISTRY}/${PROJECT_NAME}/cdci-with-go:latest
	docker push ${REGISTRY}/${PROJECT_NAME}/cdci-with-go:latest
