FROM golang:1.13.1 as builder

RUN mkdir -p $GOPATH/src/gitlab.com/samandarobidovfrd/cicd-with-go
WORKDIR $GOPATH/src/gitlab.com/samandarobidovfrd/cicd-with-go


COPY . ./

RUN export CGO_ENABLED=0 && \ 
    export GOOS=linux && \
    make build && \
    mv ./bin/cicd-with-go /


FROM alpine
COPY --from=builder cicd-with-go .
ENTRYPOINT [ "/cicd-with-go" ]